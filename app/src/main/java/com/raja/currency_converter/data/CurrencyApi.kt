package com.raja.currency_converter.data

import com.raja.currency_converter.data.model.CurrencyApiResponse
import retrofit2.Response
import retrofit2.http.GET

interface CurrencyApi {

    @GET("/latest")
    suspend fun getRates(): Response<CurrencyApiResponse>
}