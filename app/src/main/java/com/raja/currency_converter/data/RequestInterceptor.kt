package com.raja.currency_converter.data

import com.raja.currency_converter.util.Constants.Companion.API_KEY
import com.raja.currency_converter.util.Constants.Companion.KEY_NAME
import okhttp3.Interceptor
import okhttp3.Response

class RequestInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val originalHttpUrl = original.url
        val url = originalHttpUrl.newBuilder()
            .addQueryParameter(KEY_NAME, API_KEY)
            .build()
        val request = original.newBuilder().url(url).build()
        return chain.proceed(request)
    }
}