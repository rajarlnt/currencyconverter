package com.raja.currency_converter.data.model

data class CurrencyApiResponse(
    val base: String? = null,
    val date: String? = null,
    val rates: MutableMap<String, Double>? = null,
    val success: Boolean? = null,
    val timestamp: Int? = null
)