package com.raja.currency_converter.repository

import com.raja.currency_converter.data.model.CurrencyApiResponse
import com.raja.currency_converter.util.Resource

interface CurrencyConverterRepository {

    suspend fun getRates(): Resource<CurrencyApiResponse>
}