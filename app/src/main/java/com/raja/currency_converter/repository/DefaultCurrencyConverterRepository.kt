package com.raja.currency_converter.repository

import com.raja.currency_converter.data.CurrencyApi
import com.raja.currency_converter.data.model.CurrencyApiResponse
import com.raja.currency_converter.util.Constants.Companion.ERROR_OCCURRED
import com.raja.currency_converter.util.Resource
import java.lang.Exception
import javax.inject.Inject

class DefaultCurrencyConverterRepository @Inject constructor(
    private val api: CurrencyApi
) : CurrencyConverterRepository {

    override suspend fun getRates(): Resource<CurrencyApiResponse> {
        return try {
            val response = api.getRates()
            val result = response.body()
            if (response.isSuccessful && result != null) {
                Resource.Success(result)
            } else {
                Resource.Error(response.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: ERROR_OCCURRED)
        }
    }
}