package com.raja.currency_converter.ui

import android.graphics.Color
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.flow.collect
import com.raja.currency_converter.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CurrencyConverterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val viewModel: CurrencyConverterViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnCurrencyConvert.setOnClickListener {
            viewModel.convert(
                binding.etFromCurrency.text.toString(),
                binding.spFromCurrency.selectedItem.toString(),
                binding.spToCurrency.selectedItem.toString()
            )
        }

        lifecycleScope.launchWhenStarted {
            viewModel.conversion.collect { event ->
                when (event) {
                    is CurrencyConverterViewModel.CurrencyEvent.Success -> {
                        binding.progressBarCurrencyConverter.isVisible = false
                        binding.tvCurrencyResult.setTextColor(Color.WHITE)
                        binding.tvCurrencyResult.text = event.resultText
                    }
                    is CurrencyConverterViewModel.CurrencyEvent.Failure -> {
                        binding.progressBarCurrencyConverter.isVisible = false
                        binding.tvCurrencyResult.setTextColor(Color.RED)
                        binding.tvCurrencyResult.text = event.errorText
                    }
                    is CurrencyConverterViewModel.CurrencyEvent.Loading -> {
                        binding.progressBarCurrencyConverter.isVisible = true
                    }
                    else -> Unit
                }
            }
        }
    }
}