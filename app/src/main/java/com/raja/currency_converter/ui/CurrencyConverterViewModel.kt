package com.raja.currency_converter.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.raja.currency_converter.repository.CurrencyConverterRepository
import com.raja.currency_converter.util.Constants.Companion.NOT_aVALID
import com.raja.currency_converter.util.Constants.Companion.UNEXPECTED_ERROR
import com.raja.currency_converter.util.DispatcherProvider
import com.raja.currency_converter.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.math.roundToInt

@HiltViewModel
class CurrencyConverterViewModel @Inject constructor(
    private val repository: CurrencyConverterRepository,
    private val dispatcher: DispatcherProvider
) : ViewModel() {

    sealed class CurrencyEvent {
        class Success(val resultText: String) : CurrencyEvent()
        class Failure(val errorText: String) : CurrencyEvent()
        object Loading : CurrencyEvent()
        object Empty : CurrencyEvent()
    }

    private val _conversion = MutableStateFlow<CurrencyEvent>(CurrencyEvent.Empty)
    val conversion: StateFlow<CurrencyEvent> = _conversion

    fun convert(
        amountStr: String,
        fromCurrency: String,
        toCurrency: String
    ) {
        val fromAmount = amountStr.toFloatOrNull()
        if (fromAmount == null) {
            _conversion.value = CurrencyEvent.Failure(NOT_aVALID)
            return
        }

        viewModelScope.launch(dispatcher.io) {
            _conversion.value = CurrencyEvent.Loading
            when (val ratesResponse = repository.getRates()) {
                is Resource.Error -> _conversion.value =
                    CurrencyEvent.Failure(ratesResponse.message!!)
                is Resource.Success -> {
                    val rates = ratesResponse.data!!.rates
                    val toRate = rates?.get(toCurrency)
                    val fromRate = rates?.get(fromCurrency)
                    if (toRate == null || fromRate == null) {
                        _conversion.value = CurrencyEvent.Failure(UNEXPECTED_ERROR)
                    } else {
                        val convertedCurrency: Float =
                            (fromAmount / fromRate * toRate * 100).roundToInt() / 100.0F
                        _conversion.value = CurrencyEvent.Success(
                            "$fromAmount $fromCurrency = $convertedCurrency $toCurrency"
                        )
                    }
                }
            }
        }
    }
}