package com.raja.currency_converter.util

class Constants {

    companion object {

        const val API_KEY = "97f5346c1f19d53c01a5a5c70a806398"
        const val KEY_NAME = "access_key"
        const val BASE_URL = "http://api.exchangeratesapi.io/"
        const val NOT_aVALID = "Not a valid Amount!!!"
        const val UNEXPECTED_ERROR = "Unexpected Error"
        const val ERROR_OCCURRED = "An error occurred"

    }
}